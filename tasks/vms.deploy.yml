---
# update role namespace
- name: 'role.namespace.update'
  set_fact:
    role_namespace: '{{ run_mode|upper }}'


- name: 'group.local.tasks'
  block:

  # hcloud: ensure defined ssh pubkeys exists
  - name: '{{ role_namespace }}.setup.pubkeys'
    hcloud_ssh_key:
      token: '{{ hcloud_key }}'
      name: '{{ item.name }}'
      public_key: '{{ item.key }}'
    with_items: '{{ vms_server_keys }}'

  # hcloud: create servers if not exists
  - name: '{{ role_namespace }}.setup.servers'
    hcloud_server:
      token: '{{ hcloud_key }}'
      name: '{{ SERVER_PROPERTIES.name }}'
      image: '{{ SERVER_PROPERTIES.image }}'
      server_type: '{{ SERVER_PROPERTIES.server_type }}'
      ssh_keys: '{{ SERVER_PROPERTIES.server_keys }}'
      user_data: '{{ SERVER_PROPERTIES.user_data }}'
    register: vmsCreated
    # wait for running status
    until: (vmsCreated.servers[0].status == "running")
    retries: 6
    delay: 10

  # save vms properties after creation
  - name: '{{ role_namespace }}.save.VM_PROPERTIES'
    set_fact:
      # create VM_PROPERTIES
      VM_PROPERTIES: '{{ vmsCreated.servers[0] }}'
    when:
      - (VM_PROPERTIES is not defined)

  # one-time display of user credentials
  - name: '{{ role_namespace }}.show.default.user.credentials'
    debug:
      msg: 'DEFAULT CREDENTIALS for USER {{ vms_default_user }} is [{{ vms_default_pass }}]'

# block settings
  delegate_to: localhost


# check & update ssh port
- name: '{{ role_namespace }}.update.ssh.port'
  include_tasks: 'vms.update.ssh.port.yml'
  when:
    - (SERVER_PROPERTIES.port|int != 22)

# set ansible_host to server ip for next tasks
- name: '{{ role_namespace }}.update.ansible_host.to.ip'
  set_fact:
    ansible_host: '{{ VM_PROPERTIES.public_ipv4 }}'

# set host fqdn
- name: '{{ role_namespace }}.set.host.fqdn'
  hostname:
    name: '{{ SERVER_PROPERTIES.fqdn }}'

# setup dns entry for host fqdn
- name: '{{ role_namespace }}.set.host.dns.entry'
  include_role:
    name: 'dns.{{ dns_provider }}'
  vars:
    record_name: '{{ SERVER_PROPERTIES.fqdn }}'
    record_type: 'A'
    record_value: '{{ VM_PROPERTIES.public_ipv4 }}'
  when:
    - (dns_manage_fqdn|default(False))


# setup hcloud cli
- name: '{{ role_namespace }}.deploy.hcloud.cli'
  include_tasks: 'vms.deploy.hcloud-cli.yml'


# setup hcloud volumes
- name: '{{ role_namespace }}.deploy.hcloud.volumes'
  include_tasks: 'vms.deploy.hcloud-volume.yml'
  with_items: '{{ vms_volumes }}'
  loop_control:
    loop_var: 'THIS_VOLUME'
    label: '{{ THIS_VOLUME.name }}-{{ inventory_hostname }}'
  when:
    - (THIS_VOLUME.dst_hosts is not defined) or (inventory_hostname in THIS_VOLUME.dst_hosts)


# eof
